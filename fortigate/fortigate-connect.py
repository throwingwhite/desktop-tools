#! /usr/bin/env python3

import re, pexpect, time, os, subprocess, getpass, pathlib
from sys import *

''' 
Read config file
    args:
        vpuser
        vpnserver
        vpnport
'''
with open("./config") as f:
    exec(f.read())


#Enable split tunneling (default is no split tunneling )
def tunnel_routes():
   
    #Obtain next-hop for tunnel interface (next-hop not consistent across connections) 
    nexthop = subprocess.Popen('ip route | grep "default.*pp0" | egrep -o "([0-9]*\.){3}[0-9]{0,3}"', shell=True, stdout=subprocess.PIPE).stdout.read().decode()
    
    # Remove default tunnel route and add more specific routes
    subprocess.Popen('sudo /usr/sbin/ip route del default via '+nexthop, shell=True, stdout=subprocess.PIPE) 
    subprocess.Popen('sudo /usr/sbin/ip route add 10.10.200.0/24 via '+nexthop, shell=True, stdout=subprocess.PIPE)
    subprocess.Popen('sudo /usr/sbin/ip route add 10.10.250.0/24 via '+nexthop, shell=True, stdout=subprocess.PIPE)
    subprocess.Popen('sudo /usr/sbin/ip route add 192.168.190.0/24 via '+nexthop, shell=True, stdout=subprocess.PIPE)
    subprocess.Popen('sudo /usr/sbin/ip route add 192.168.250.0/24 via '+nexthop, shell=True, stdout=subprocess.PIPE)

def clean_dns():
    #Clean up DNS by removing VPN inherited resolvers
    subprocess.Popen('sudo /bin/sed -i \'/.*\(89.248\|217.196\).*/d\' /etc/resolv.conf', shell=True, stdout=subprocess.PIPE)

''' Check PID is running 
Return PID if true otherwise False '''
def fStatus(file):
    try:
        pid = int(open(file).read())
        os.kill(pid,0)
        return pid
    except:
        return False
''' Kill PID if runnning
Return PID if killed otherwise False '''
def fKill(file):
    #try:
        pid = int(open(file).read())
        os.kill(pid,9)
        os.remove(file)
        return pid
    #except:
    #    return False

def main():

    usage="Usage: forticlient-connect.py [kill|status|split]"

    arg = ''
    argc = len(argv)
    
    # check args
    if argc > 1:
        
        if argc > 2:
            print(usage)
            exit(1)
    
        arg=argv[1]
    
        if arg == 'status':
            status = fStatus(pidpath)
            if status:
                print('Forticlient is running (PID: '+str(status)+')')
                exit()
            else:
                print(pidpath+' does not exist')
                exit(1)
        elif arg == 'kill':
            kill = fKill(pidpath)
            if kill:
                print('Forticlient terminated (PID: '+str(kill)+')')
                clean_dns()
                exit()
            else:
                print(pidpath+' does not exist')
                exit(1)
        elif arg == 'split':
            pass
        else:
            print(usage)
            exit(1)
 
    vpnpass = getpass.getpass('Please enter password for '+vpnuser+': ')


    pid = os.fork()

    if not pid:

        # write pid to pidpath
        fpid = os.getpid()
        pidfile = open(pidpath,'w')
        pidfile.write(str(fpid))
        pidfile.close()
             
        # execute Forticlient with args - client only accepts password arg from STDIN
        console = pexpect.spawn(forticlient_path+' --server '+vpnserver+':'+vpnport+' --vpnuser '+vpnuser)

        console.expect('Password for VPN:')

        console.sendline(vpnpass+"\n")

        console.expect('connect to this server')

        console.sendline('Y\n')
        
        #Sleep to allow VPN to connect and inherit routes
        time.sleep(5)
        
        #remove tunnel routes and DNS resolvers    
        if arg == "split":
            tunnel_routes()
            clean_dns()
        
        console.wait()

        clean_dns()
        fkill()   
 
main()

